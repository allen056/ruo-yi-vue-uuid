# RuoYi-Vue-UUID

#### 介绍
基于RuoYi-Vue V3.8.4,改造框架主键为UUID

#### 改动功能
- 1、修改主键为UUID
- 2、数据字典项支持树状目录
- 3、修改ruoyi初始化SQL语句

