-- ----------------------------
-- 1、部门表
-- ----------------------------
drop table if exists sys_dept;
create table sys_dept
(
    dept_id     varchar(36) not null comment '部门id',
    parent_id   varchar(36)  default '0' comment '父部门id',
    ancestors   varchar(500) default '' comment '祖级列表',
    dept_name   varchar(30)  default '' comment '部门名称',
    order_num   int(4)       default 0 comment '显示顺序',
    leader      varchar(20)  default null comment '负责人',
    phone       varchar(11)  default null comment '联系电话',
    email       varchar(50)  default null comment '邮箱',
    status      char(1)      default '0' comment '部门状态（0正常 1停用）',
    del_flag    char(1)      default '0' comment '删除标志（0代表存在 2代表删除）',
    create_by   varchar(64)  default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64)  default '' comment '更新者',
    update_time datetime comment '更新时间',
    primary key (dept_id)
) engine = innodb comment = '部门表';

-- ----------------------------
-- 初始化-部门表数据
-- ----------------------------
insert into sys_dept
values ('df04b486-831a-49e6-bf97-9393178594fd', '0', '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0',
        'admin', sysdate(), '', null);
insert into sys_dept
values ('3f6cc270-0438-4df6-8828-4cae5f7d4ebf', 'df04b486-831a-49e6-bf97-9393178594fd',
        '0,df04b486-831a-49e6-bf97-9393178594fd', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin',
        sysdate(), '', null);
insert into sys_dept
values ('76da3330-6c44-4801-82c8-901770cd85c5', '3f6cc270-0438-4df6-8828-4cae5f7d4ebf',
        '0,df04b486-831a-49e6-bf97-9393178594fd,3f6cc270-0438-4df6-8828-4cae5f7d4ebf', '研发部门', 1, '若依', '15888888888',
        'ry@qq.com', '0', '0', 'admin', sysdate(), '', null);
insert into sys_dept
values ('48d31614-8826-452f-921b-380a8e8f761e', '3f6cc270-0438-4df6-8828-4cae5f7d4ebf',
        '0,df04b486-831a-49e6-bf97-9393178594fd,3f6cc270-0438-4df6-8828-4cae5f7d4ebf', '测试部门', 3, '若依', '15888888888',
        'ry@qq.com', '0', '0', 'admin', sysdate(), '', null);


-- ----------------------------
-- 2、用户信息表
-- ----------------------------
drop table if exists sys_user;
create table sys_user
(
    user_id     varchar(36) not null comment '用户ID',
    dept_id     varchar(36)  default null comment '部门ID',
    user_name   varchar(30) not null comment '用户账号',
    nick_name   varchar(30) not null comment '用户昵称',
    user_type   varchar(2)   default '00' comment '用户类型（00系统用户）',
    email       varchar(50)  default '' comment '用户邮箱',
    phonenumber varchar(11)  default '' comment '手机号码',
    sex         char(1)      default '0' comment '用户性别（0男 1女 2未知）',
    avatar      varchar(100) default '' comment '头像地址',
    password    varchar(100) default '' comment '密码',
    status      char(1)      default '0' comment '帐号状态（0正常 1停用）',
    del_flag    char(1)      default '0' comment '删除标志（0代表存在 2代表删除）',
    login_ip    varchar(128) default '' comment '最后登录IP',
    login_date  datetime comment '最后登录时间',
    create_by   varchar(64)  default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64)  default '' comment '更新者',
    update_time datetime comment '更新时间',
    remark      varchar(500) default null comment '备注',
    primary key (user_id)
) engine = innodb comment = '用户信息表';

-- ----------------------------
-- 初始化-用户信息表数据
-- ----------------------------
insert into sys_user
values ('1', '76da3330-6c44-4801-82c8-901770cd85c5', 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '',
        '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', sysdate(), 'admin',
        sysdate(), '', null, '管理员');


-- ----------------------------
-- 3、岗位信息表
-- ----------------------------
drop table if exists sys_post;
create table sys_post
(
    post_id     varchar(36) not null comment '岗位ID',
    post_code   varchar(64) not null comment '岗位编码',
    post_name   varchar(50) not null comment '岗位名称',
    post_sort   int(4)      not null comment '显示顺序',
    status      char(1)     not null comment '状态（0正常 1停用）',
    create_by   varchar(64)  default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64)  default '' comment '更新者',
    update_time datetime comment '更新时间',
    remark      varchar(500) default null comment '备注',
    primary key (post_id)
) engine = innodb comment = '岗位信息表';

-- ----------------------------
-- 初始化-岗位信息表数据
-- ----------------------------
insert into sys_post
values ('53bad309-6183-4dcd-8f65-b1b3b3ff0a45', 'ceo', '董事长', 1, '0', 'admin', sysdate(), '', null, '');



-- ----------------------------
-- 4、角色信息表
-- ----------------------------
drop table if exists sys_role;
create table sys_role
(
    role_id             varchar(36)  not null comment '角色ID',
    role_name           varchar(30)  not null comment '角色名称',
    role_key            varchar(100) not null comment '角色权限字符串',
    role_sort           int(4)       not null comment '显示顺序',
    data_scope          char(1)      default '1' comment '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
    menu_check_strictly tinyint(1)   default 1 comment '菜单树选择项是否关联显示',
    dept_check_strictly tinyint(1)   default 1 comment '部门树选择项是否关联显示',
    status              char(1)      not null comment '角色状态（0正常 1停用）',
    del_flag            char(1)      default '0' comment '删除标志（0代表存在 2代表删除）',
    create_by           varchar(64)  default '' comment '创建者',
    create_time         datetime comment '创建时间',
    update_by           varchar(64)  default '' comment '更新者',
    update_time         datetime comment '更新时间',
    remark              varchar(500) default null comment '备注',
    primary key (role_id)
) engine = innodb comment = '角色信息表';

-- ----------------------------
-- 初始化-角色信息表数据
-- ----------------------------
insert into sys_role
values ('1', '超级管理员', 'admin', 1, 1, 1, 1, '0', '0', 'admin', sysdate(), '', null,
        '超级管理员');
insert into sys_role
values ('53bad309-6183-4dcd-8f65-b1b3b3ff0a45', '普通角色', 'common', 2, 2, 1, 1, '0', '0', 'admin', sysdate(), '', null,
        '普通角色');


-- ----------------------------
-- 5、菜单权限表
-- ----------------------------
drop table if exists sys_menu;
create table sys_menu
(
    menu_id     varchar(36) not null comment '菜单ID',
    menu_name   varchar(50) not null comment '菜单名称',
    parent_id   varchar(36)  default '0' comment '父菜单ID',
    order_num   int(4)       default 0 comment '显示顺序',
    path        varchar(200) default '' comment '路由地址',
    component   varchar(255) default null comment '组件路径',
    query       varchar(255) default null comment '路由参数',
    is_frame    int(1)       default 1 comment '是否为外链（0是 1否）',
    is_cache    int(1)       default 0 comment '是否缓存（0缓存 1不缓存）',
    menu_type   char(1)      default '' comment '菜单类型（M目录 C菜单 F按钮）',
    visible     char(1)      default 0 comment '菜单状态（0显示 1隐藏）',
    status      char(1)      default 0 comment '菜单状态（0正常 1停用）',
    perms       varchar(100) default null comment '权限标识',
    icon        varchar(100) default '#' comment '菜单图标',
    create_by   varchar(64)  default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64)  default '' comment '更新者',
    update_time datetime comment '更新时间',
    remark      varchar(500) default '' comment '备注',
    primary key (menu_id)
) engine = innodb comment = '菜单权限表';

-- ----------------------------
-- 初始化-菜单信息表数据
-- ----------------------------
-- 一级菜单
insert into sys_menu
values ('66a3de94-e5f3-4d25-a00b-fb31bedf5be2', '系统管理', '0', '1', 'system', null, '', 1, 0, 'M', '0', '0', '', 'system',
        'admin', sysdate(), '', null,
        '系统管理目录');
insert into sys_menu
values ('26085024-5ea9-4c89-ba38-54c50b5f5cf7', '系统监控', '0', '2', 'monitor', null, '', 1, 0, 'M', '0', '0', '',
        'monitor', 'admin', sysdate(), '', null,
        '系统监控目录');
insert into sys_menu
values ('4783678a-bf6a-4e18-b295-043385643551', '系统工具', '0', '3', 'tool', null, '', 1, 0, 'M', '0', '0', '', 'tool',
        'admin', sysdate(), '', null,
        '系统工具目录');
-- 二级菜单
insert into sys_menu
values ('79432594-fc34-4f96-a33b-9ba03ba85719', '用户管理', '66a3de94-e5f3-4d25-a00b-fb31bedf5be2', '1', 'user',
        'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user',
        'admin', sysdate(), '', null, '用户管理菜单');
insert into sys_menu
values ('00b15389-62f7-429f-a64d-aedd71545537', '角色管理', '66a3de94-e5f3-4d25-a00b-fb31bedf5be2', '2', 'role',
        'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples',
        'admin', sysdate(), '', null, '角色管理菜单');
insert into sys_menu
values ('d697209c-c107-4d60-ba74-0b7d401068d6', '菜单管理', '66a3de94-e5f3-4d25-a00b-fb31bedf5be2', '3', 'menu',
        'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table',
        'admin', sysdate(), '', null, '菜单管理菜单');
insert into sys_menu
values ('6544463a-a680-4f15-97aa-1ed01273fdaa', '部门管理', '66a3de94-e5f3-4d25-a00b-fb31bedf5be2', '4', 'dept',
        'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree',
        'admin', sysdate(), '', null, '部门管理菜单');
insert into sys_menu
values ('9b2f8c6e-c522-4e55-89f3-43bf64dc184c', '岗位管理', '66a3de94-e5f3-4d25-a00b-fb31bedf5be2', '5', 'post',
        'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post',
        'admin', sysdate(), '', null, '岗位管理菜单');
insert into sys_menu
values ('6669f519-25b1-44cc-986c-c049451156d2', '字典管理', '66a3de94-e5f3-4d25-a00b-fb31bedf5be2', '6', 'dict',
        'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict',
        'admin', sysdate(), '', null, '字典管理菜单');
insert into sys_menu
values ('c470ea9a-fca3-4aa0-b645-d00886888765', '参数设置', '66a3de94-e5f3-4d25-a00b-fb31bedf5be2', '7', 'config',
        'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit',
        'admin', sysdate(), '', null, '参数设置菜单');
insert into sys_menu
values ('e0a1dcb9-d171-48c4-8650-e59e52b05c44', '通知公告', '66a3de94-e5f3-4d25-a00b-fb31bedf5be2', '8', 'notice',
        'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list',
        'message', 'admin', sysdate(), '', null, '通知公告菜单');
insert into sys_menu
values ('e2bdaf96-8438-4514-a4e8-4b830b69bd32', '日志管理', '66a3de94-e5f3-4d25-a00b-fb31bedf5be2', '9', 'log', '', '', 1,
        0, 'M', '0', '0', '', 'log', 'admin', sysdate(), '', null, '日志管理菜单');
insert into sys_menu
values ('2107221e-2fde-436b-86a3-195f4251012c', '在线用户', '26085024-5ea9-4c89-ba38-54c50b5f5cf7', '1', 'online',
        'monitor/online/index', '', 1, 0, 'C',
        '0', '0', 'monitor:online:list',
        'online', 'admin', sysdate(), '', null, '在线用户菜单');
insert into sys_menu
values ('0496f4b2-7df2-4284-97ba-f384269f3cf3', '定时任务', '26085024-5ea9-4c89-ba38-54c50b5f5cf7', '2', 'job',
        'monitor/job/index', '', 1, 0, 'C', '0', '0',
        'monitor:job:list', 'job',
        'admin', sysdate(), '', null, '定时任务菜单');
insert into sys_menu
values ('ed5e2666-810f-4027-89dd-67de8f4d3c53', '数据监控', '26085024-5ea9-4c89-ba38-54c50b5f5cf7', '3', 'druid',
        'monitor/druid/index', '', 1, 0, 'C', '0',
        '0', 'monitor:druid:list', 'druid',
        'admin', sysdate(), '', null, '数据监控菜单');
insert into sys_menu
values ('16df2cfd-d1ac-4a96-b0e8-d0e2c4cad912', '服务监控', '26085024-5ea9-4c89-ba38-54c50b5f5cf7', '4', 'server',
        'monitor/server/index', '', 1, 0, 'C',
        '0', '0', 'monitor:server:list',
        'server', 'admin', sysdate(), '', null, '服务监控菜单');
insert into sys_menu
values ('2a6d77e2-4dfd-49c1-95ae-98c1829974d8', '缓存监控', '26085024-5ea9-4c89-ba38-54c50b5f5cf7', '5', 'cache',
        'monitor/cache/index', '', 1, 0, 'C', '0',
        '0', 'monitor:cache:list', 'redis',
        'admin', sysdate(), '', null, '缓存监控菜单');
insert into sys_menu
values ('67403204-5602-4ed4-a560-083f31bf9831', '缓存列表', '26085024-5ea9-4c89-ba38-54c50b5f5cf7', '6', 'cacheList',
        'monitor/cache/list', '', 1, 0, 'C',
        '0', '0', 'monitor:cache:list',
        'redis-list', 'admin', sysdate(), '', null, '缓存列表菜单');
insert into sys_menu
values ('92d1fc04-1a56-4ff8-9af2-0069dd4245c8', '表单构建', '4783678a-bf6a-4e18-b295-043385643551', '1', 'build',
        'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build',
        'admin', sysdate(), '', null, '表单构建菜单');
insert into sys_menu
values ('5de6a821-d269-4c32-aa63-20bf5694c5c0', '代码生成', '4783678a-bf6a-4e18-b295-043385643551', '2', 'gen',
        'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin',
        sysdate(), '', null, '代码生成菜单');
insert into sys_menu
values ('72657b5c-65f3-4090-b501-0775ddc61b28', '系统接口', '4783678a-bf6a-4e18-b295-043385643551', '3', 'swagger',
        'tool/swagger/index', '', 1, 0, 'C', '0', '0', 'tool:swagger:list',
        'swagger', 'admin', sysdate(), '', null, '系统接口菜单');
-- 三级菜单
insert into sys_menu
values ('410d8dcb-bb4f-4b40-b2e7-6a842b3dc404', '操作日志', 'e2bdaf96-8438-4514-a4e8-4b830b69bd32', '1', 'operlog',
        'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list',
        'form', 'admin', sysdate(), '', null, '操作日志菜单');
insert into sys_menu
values ('e8a5e7d7-1c80-4fa0-b7c9-91bffeefe328', '登录日志', 'e2bdaf96-8438-4514-a4e8-4b830b69bd32', '2', 'logininfor',
        'monitor/logininfor/index', '', 1, 0, 'C', '0', '0',
        'monitor:logininfor:list', 'logininfor', 'admin', sysdate(), '', null, '登录日志菜单');



-- ----------------------------
-- 6、用户和角色关联表  用户N-1角色
-- ----------------------------
drop table if exists sys_user_role;
create table sys_user_role
(
    user_id varchar(36) not null comment '用户ID',
    role_id varchar(36) not null comment '角色ID',
    primary key (user_id, role_id)
) engine = innodb comment = '用户和角色关联表';

-- ----------------------------
-- 初始化-用户和角色关联表数据
-- ----------------------------
insert into sys_user_role
values ('1', '63fa158a-01f4-4e06-9ff6-94d07ee7fa5d');


-- ----------------------------
-- 7、角色和菜单关联表  角色1-N菜单
-- ----------------------------
drop table if exists sys_role_menu;
create table sys_role_menu
(
    role_id varchar(36) not null comment '角色ID',
    menu_id varchar(36) not null comment '菜单ID',
    primary key (role_id, menu_id)
) engine = innodb comment = '角色和菜单关联表';


-- ----------------------------
-- 8、角色和部门关联表  角色1-N部门
-- ----------------------------
drop table if exists sys_role_dept;
create table sys_role_dept
(
    role_id varchar(36) not null comment '角色ID',
    dept_id varchar(36) not null comment '部门ID',
    primary key (role_id, dept_id)
) engine = innodb comment = '角色和部门关联表';

-- ----------------------------
-- 初始化-角色和部门关联表数据
-- ----------------------------


-- ----------------------------
-- 9、用户与岗位关联表  用户1-N岗位
-- ----------------------------
drop table if exists sys_user_post;
create table sys_user_post
(
    user_id varchar(36) not null comment '用户ID',
    post_id varchar(36) not null comment '岗位ID',
    primary key (user_id, post_id)
) engine = innodb comment = '用户与岗位关联表';

-- ----------------------------
-- 初始化-用户与岗位关联表数据
-- ----------------------------
insert into sys_user_post
values ('1', '53bad309-6183-4dcd-8f65-b1b3b3ff0a45');


-- ----------------------------
-- 10、操作日志记录
-- ----------------------------
drop table if exists sys_oper_log;
create table sys_oper_log
(
    oper_id        varchar(36) not null comment '日志主键',
    title          varchar(50)   default '' comment '模块标题',
    business_type  int(2)        default 0 comment '业务类型（0其它 1新增 2修改 3删除）',
    method         varchar(100)  default '' comment '方法名称',
    request_method varchar(10)   default '' comment '请求方式',
    operator_type  int(1)        default 0 comment '操作类别（0其它 1后台用户 2手机端用户）',
    oper_name      varchar(50)   default '' comment '操作人员',
    dept_name      varchar(50)   default '' comment '部门名称',
    oper_url       varchar(255)  default '' comment '请求URL',
    oper_ip        varchar(128)  default '' comment '主机地址',
    oper_location  varchar(255)  default '' comment '操作地点',
    oper_param     varchar(2000) default '' comment '请求参数',
    json_result    varchar(2000) default '' comment '返回参数',
    status         int(1)        default 0 comment '操作状态（0正常 1异常）',
    error_msg      varchar(2000) default '' comment '错误消息',
    oper_time      datetime comment '操作时间',
    primary key (oper_id)
) engine = innodb comment = '操作日志记录';


-- ----------------------------
-- 11、字典类型表
-- ----------------------------
drop table if exists sys_dict_type;
create table sys_dict_type
(
    dict_id     varchar(36) not null comment '字典主键',
    dict_name   varchar(100) default '' comment '字典类型名称',
    dict_type   varchar(100) default '' comment '字典类型显示名称',
    status      char(1)      default '0' comment '状态（0正常 1停用）',
    create_by   varchar(64)  default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64)  default '' comment '更新者',
    update_time datetime comment '更新时间',
    remark      varchar(500) default null comment '备注',
    primary key (dict_id),
    unique (dict_type)
) engine = innodb comment = '字典类型表';

insert into sys_dict_type
values ('89fdb1b3-cca9-4a46-b84c-34facad0615f', '用户性别', 'sys_user_sex', '0', 'admin', sysdate(), '', null, '用户性别列表');
insert into sys_dict_type
values ('978f45bc-0996-4388-8bc2-9b4d004aba36', '菜单状态', 'sys_show_hide', '0', 'admin', sysdate(), '', null, '菜单状态列表');
insert into sys_dict_type
values ('0719e66a-861a-42fb-85b7-c2330083b4ba', '系统开关', 'sys_normal_disable', '0', 'admin', sysdate(), '', null,
        '系统开关列表');
insert into sys_dict_type
values ('f43b7ec1-a039-4d63-8fe0-106c8ee45685', '任务状态', 'sys_job_status', '0', 'admin', sysdate(), '', null, '任务状态列表');
insert into sys_dict_type
values ('706222c5-6719-45c1-9217-9bcaf554be35', '任务分组', 'sys_job_group', '0', 'admin', sysdate(), '', null, '任务分组列表');
insert into sys_dict_type
values ('1f515ef3-72a9-4e03-bd9b-b0d6a6e3fd16', '系统是否', 'sys_yes_no', '0', 'admin', sysdate(), '', null, '系统是否列表');
insert into sys_dict_type
values ('721f5b43-30d4-4e8e-be9d-463c08bc6fe5', '通知类型', 'sys_notice_type', '0', 'admin', sysdate(), '', null, '通知类型列表');
insert into sys_dict_type
values ('16e6defe-ed47-4bf6-8cff-201a7deeeb5d', '通知状态', 'sys_notice_status', '0', 'admin', sysdate(), '', null,
        '通知状态列表');
insert into sys_dict_type
values ('b244ab3d-916c-42ab-8836-b5bc9a766aa8', '操作类型', 'sys_oper_type', '0', 'admin', sysdate(), '', null, '操作类型列表');
insert into sys_dict_type
values ('292669e5-784e-40e8-a310-1e8e7f472a19', '系统状态', 'sys_common_status', '0', 'admin', sysdate(), '', null,
        '登录状态列表');


-- ----------------------------
-- 12、字典数据表
-- ----------------------------
drop table if exists sys_dict_data;
create table sys_dict_data
(
    dict_code   varchar(36) not null comment '字典编码',
    parent_id   varchar(36)  default '' comment '父ID',
    dict_sort   int(4)       default 0 comment '字典排序',
    dict_label  varchar(100) default '' comment '字典标签',
    dict_value  varchar(100) default '' comment '字典键值',
    dict_type   varchar(100) default '' comment '字典类型',
    css_class   varchar(100) default null comment '样式属性（其他样式扩展）',
    list_class  varchar(100) default null comment '表格回显样式',
    is_default  char(1)      default 'N' comment '是否默认（Y是 N否）',
    status      char(1)      default '0' comment '状态（0正常 1停用）',
    create_by   varchar(64)  default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64)  default '' comment '更新者',
    update_time datetime comment '更新时间',
    remark      varchar(500) default null comment '备注',
    primary key (dict_code)
) engine = innodb comment = '字典数据表';

insert into sys_dict_data
values ('17da22b0-a1e7-45ec-a078-a035d53b3c95', '', 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', sysdate(),
        '', null, '性别男');
insert into sys_dict_data
values ('d9e14e3d-29c1-4247-8ad5-963a63033118', '', 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', sysdate(),
        '', null, '性别女');
insert into sys_dict_data
values ('d16d960a-0544-4de2-b75a-364d91505880', '', 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', sysdate(),
        '', null, '性别未知');
insert into sys_dict_data
values ('bb8cfa2c-c495-45c5-8a1e-42169643932b', '', 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin',
        sysdate(), '', null, '显示菜单');
insert into sys_dict_data
values ('aee8671c-2dc8-4f0e-97e9-dd62af6873c2', '', 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin',
        sysdate(), '', null, '隐藏菜单');
insert into sys_dict_data
values ('c5c15f6b-a0de-4b84-9282-6b425502b700', '', 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0',
        'admin', sysdate(), '', null, '正常状态');
insert into sys_dict_data
values ('e19d134e-69ca-468f-ba4f-71987360829a', '', 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin',
        sysdate(), '', null, '停用状态');
insert into sys_dict_data
values ('1ae986be-62d7-4865-a782-b154a85d87e4', '', 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin',
        sysdate(), '', null, '正常状态');
insert into sys_dict_data
values ('9cdefeb6-0001-44db-998a-0ea9a94ca7c3', '', 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin',
        sysdate(), '', null, '停用状态');
insert into sys_dict_data
values ('938e83db-b5b0-4606-ae2e-59994768043d', '', 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin',
        sysdate(), '', null, '默认分组');
insert into sys_dict_data
values ('fd88260f-bc73-4c72-87ca-587ec4058055', '', 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin',
        sysdate(), '', null, '系统分组');
insert into sys_dict_data
values ('f979c3df-a16d-4c7b-8d8b-4c32f310d2f0', '', 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin',
        sysdate(), '', null, '系统默认是');
insert into sys_dict_data
values ('353ffa7a-14d7-4986-9139-db5f83f800bb', '', 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin',
        sysdate(), '', null, '系统默认否');
insert into sys_dict_data
values ('606a5492-4c43-4400-98eb-986b91e6f677', '', 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin',
        sysdate(), '', null, '通知');
insert into sys_dict_data
values ('897f587b-febe-43b8-b123-1f3bfb3c7f77', '', 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin',
        sysdate(), '', null, '公告');
insert into sys_dict_data
values ('a191e017-bef6-46d9-8188-0e6009f9c6de', '', 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin',
        sysdate(), '', null, '正常状态');
insert into sys_dict_data
values ('4992dc08-caca-44a2-ae9f-7ace91589ab4', '', 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin',
        sysdate(), '', null, '关闭状态');
insert into sys_dict_data
values ('6e2036b0-adee-4dbd-b9d4-6b06e35351b2', '', 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin',
        sysdate(), '', null, '其他操作');
insert into sys_dict_data
values ('f3093ea0-7967-4e3b-a753-67acb1c7039a', '', 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin',
        sysdate(), '', null, '新增操作');
insert into sys_dict_data
values ('93899ec6-7ad3-4d89-9c9f-fd7b6cd7658b', '', 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin',
        sysdate(), '', null, '修改操作');
insert into sys_dict_data
values ('84830bf5-12b4-41bb-aa89-1ff0ad9a19cc', '', 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin',
        sysdate(), '', null, '删除操作');
insert into sys_dict_data
values ('a25b6204-96e9-458a-a47c-449a3845ac4c', '', 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin',
        sysdate(), '', null, '授权操作');
insert into sys_dict_data
values ('56f0aa33-7426-4d58-8450-856172983057', '', 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin',
        sysdate(), '', null, '导出操作');
insert into sys_dict_data
values ('a177dfcc-3a36-4906-a8eb-919be5054cfa', '', 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin',
        sysdate(), '', null, '导入操作');
insert into sys_dict_data
values ('8b625b00-97fa-4076-98a8-3a48a463170d', '', 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin',
        sysdate(), '', null, '强退操作');
insert into sys_dict_data
values ('6023c63e-b326-4bb8-b514-f8f089faafeb', '', 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin',
        sysdate(), '', null, '生成操作');
insert into sys_dict_data
values ('f28cc0cd-f88f-4b9b-9e9d-61c01e1498e6', '', 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin',
        sysdate(), '', null, '清空操作');
insert into sys_dict_data
values ('76036e4f-2228-4bc2-8632-5f11dc9fa6f6', '', 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin',
        sysdate(), '', null, '正常状态');
insert into sys_dict_data
values ('44ca964f-9720-4b4a-b2a4-dae7a69f9ffd', '', 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin',
        sysdate(), '', null, '停用状态');
-- ----------------------------
-- 13、参数配置表
-- ----------------------------
drop table if exists sys_config;
create table sys_config
(
    config_id    varchar(36) not null comment '参数主键',
    config_name  varchar(100) default '' comment '参数名称',
    config_key   varchar(100) default '' comment '参数键名',
    config_value varchar(500) default '' comment '参数键值',
    config_type  char(1)      default 'N' comment '系统内置（Y是 N否）',
    create_by    varchar(64)  default '' comment '创建者',
    create_time  datetime comment '创建时间',
    update_by    varchar(64)  default '' comment '更新者',
    update_time  datetime comment '更新时间',
    remark       varchar(500) default null comment '备注',
    primary key (config_id)
) engine = innodb comment = '参数配置表';

insert into sys_config
values ('c36c7726-c8af-495f-9f16-73f5d5be6a3c', '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin',
        sysdate(), '', null,
        '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
insert into sys_config
values ('7b333a14-2640-4ca3-b8ab-14a60d488d61', '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin',
        sysdate(), '', null, '初始化密码 123456');
insert into sys_config
values ('19aa8ccb-44a9-4d1d-bffe-8afe4f3bb28b', '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin',
        sysdate(), '', null,
        '深色主题theme-dark，浅色主题theme-light');
insert into sys_config
values ('557e36cc-c7a5-4529-8bb8-5c4f98736e1b', '账号自助-验证码开关', 'sys.account.captchaEnabled', 'true', 'Y', 'admin',
        sysdate(), '', null,
        '是否开启验证码功能（true开启，false关闭）');
insert into sys_config
values ('8be12407-98b2-42f0-acba-928806357fa4', '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin',
        sysdate(), '', null,
        '是否开启注册用户功能（true开启，false关闭）');


-- ----------------------------
-- 14、系统访问记录
-- ----------------------------
drop table if exists sys_logininfor;
create table sys_logininfor
(
    info_id        varchar(36) not null comment '访问ID',
    user_name      varchar(50)  default '' comment '用户账号',
    ipaddr         varchar(128) default '' comment '登录IP地址',
    login_location varchar(255) default '' comment '登录地点',
    browser        varchar(50)  default '' comment '浏览器类型',
    os             varchar(50)  default '' comment '操作系统',
    status         char(1)      default '0' comment '登录状态（0成功 1失败）',
    msg            varchar(255) default '' comment '提示消息',
    login_time     datetime comment '访问时间',
    primary key (info_id)
) engine = innodb comment = '系统访问记录';


-- ----------------------------
-- 15、定时任务调度表
-- ----------------------------
drop table if exists sys_job;
create table sys_job
(
    job_id          varchar(36)  not null comment '任务ID',
    job_name        varchar(64)  default '' comment '任务名称',
    job_group       varchar(64)  default 'DEFAULT' comment '任务组名',
    invoke_target   varchar(500) not null comment '调用目标字符串',
    cron_expression varchar(255) default '' comment 'cron执行表达式',
    misfire_policy  varchar(20)  default '3' comment '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
    concurrent      char(1)      default '1' comment '是否并发执行（0允许 1禁止）',
    status          char(1)      default '0' comment '状态（0正常 1暂停）',
    create_by       varchar(64)  default '' comment '创建者',
    create_time     datetime comment '创建时间',
    update_by       varchar(64)  default '' comment '更新者',
    update_time     datetime comment '更新时间',
    remark          varchar(500) default '' comment '备注信息',
    primary key (job_id, job_name, job_group)
) engine = innodb comment = '定时任务调度表';

insert into sys_job
values ('7eefba1f-38ba-43c9-8671-979b8eca3218', '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1',
        '1', 'admin', sysdate(), '', null,
        '');
insert into sys_job
values ('11db5d09-9eef-46a7-9be7-8c6459b670bc', '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3',
        '1', '1', 'admin', sysdate(), '',
        null, '');
insert into sys_job
values ('d3044ae2-e329-417c-beed-50c878437859', '系统默认（多参）', 'DEFAULT',
        'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3',
        '1', '1', 'admin', sysdate(), '', null, '');


-- ----------------------------
-- 16、定时任务调度日志表
-- ----------------------------
drop table if exists sys_job_log;
create table sys_job_log
(
    job_log_id     varchar(36)  not null comment '任务日志ID',
    job_name       varchar(64)  not null comment '任务名称',
    job_group      varchar(64)  not null comment '任务组名',
    invoke_target  varchar(500) not null comment '调用目标字符串',
    job_message    varchar(500) comment '日志信息',
    status         char(1)       default '0' comment '执行状态（0正常 1失败）',
    exception_info varchar(2000) default '' comment '异常信息',
    create_time    datetime comment '创建时间',
    primary key (job_log_id)
) engine = innodb comment = '定时任务调度日志表';


-- ----------------------------
-- 17、通知公告表
-- ----------------------------
drop table if exists sys_notice;
create table sys_notice
(
    notice_id      varchar(36) not null comment '公告ID',
    notice_title   varchar(50) not null comment '公告标题',
    notice_type    varchar(36) not null comment '公告类型',
    notice_content longblob     default null comment '公告内容',
    status         varchar(36)  default 'a191e017-bef6-46d9-8188-0e6009f9c6de' comment '公告状态',
    create_by      varchar(64)  default '' comment '创建者',
    create_time    datetime comment '创建时间',
    update_by      varchar(64)  default '' comment '更新者',
    update_time    datetime comment '更新时间',
    remark         varchar(255) default null comment '备注',
    primary key (notice_id)
) engine = innodb comment = '通知公告表';

-- ----------------------------
-- 初始化-公告信息表数据
-- ----------------------------
insert into sys_notice
values ('3d541be9-7ade-4000-8d36-6acf5b423d9e', '温馨提醒：2018-07-01 若依新版本发布啦', '897f587b-febe-43b8-b123-1f3bfb3c7f77',
        '新版本内容', 'a191e017-bef6-46d9-8188-0e6009f9c6de', 'admin', sysdate(), '',
        null, '管理员');
insert into sys_notice
values ('a42c85f0-d76f-4191-bca3-0769887edaa9', '维护通知：2018-07-01 若依系统凌晨维护', '606a5492-4c43-4400-98eb-986b91e6f677',
        '维护内容', 'a191e017-bef6-46d9-8188-0e6009f9c6de', 'admin', sysdate(), '',
        null, '管理员');


-- ----------------------------
-- 18、代码生成业务表
-- ----------------------------
drop table if exists gen_table;
create table gen_table
(
    table_id          varchar(36) not null comment '编号',
    table_name        varchar(200) default '' comment '表名称',
    table_comment     varchar(500) default '' comment '表描述',
    sub_table_name    varchar(64)  default null comment '关联子表的表名',
    sub_table_fk_name varchar(64)  default null comment '子表关联的外键名',
    class_name        varchar(100) default '' comment '实体类名称',
    tpl_category      varchar(200) default 'crud' comment '使用的模板（crud单表操作 tree树表操作）',
    package_name      varchar(100) comment '生成包路径',
    module_name       varchar(30) comment '生成模块名',
    business_name     varchar(30) comment '生成业务名',
    function_name     varchar(50) comment '生成功能名',
    function_author   varchar(50) comment '生成功能作者',
    gen_type          char(1)      default '0' comment '生成代码方式（0zip压缩包 1自定义路径）',
    gen_path          varchar(200) default '/' comment '生成路径（不填默认项目路径）',
    options           varchar(1000) comment '其它生成选项',
    create_by         varchar(64)  default '' comment '创建者',
    create_time       datetime comment '创建时间',
    update_by         varchar(64)  default '' comment '更新者',
    update_time       datetime comment '更新时间',
    remark            varchar(500) default null comment '备注',
    primary key (table_id)
) engine = innodb comment = '代码生成业务表';


-- ----------------------------
-- 19、代码生成业务表字段
-- ----------------------------
drop table if exists gen_table_column;
create table gen_table_column
(
    column_id      varchar(36) not null comment '编号',
    table_id       varchar(64) comment '归属表编号',
    column_name    varchar(200) comment '列名称',
    column_comment varchar(500) comment '列描述',
    column_type    varchar(100) comment '列类型',
    java_type      varchar(500) comment 'JAVA类型',
    java_field     varchar(200) comment 'JAVA字段名',
    is_pk          char(1) comment '是否主键（1是）',
    is_increment   char(1) comment '是否自增（1是）',
    is_required    char(1) comment '是否必填（1是）',
    is_insert      char(1) comment '是否为插入字段（1是）',
    is_edit        char(1) comment '是否编辑字段（1是）',
    is_list        char(1) comment '是否列表字段（1是）',
    is_query       char(1) comment '是否查询字段（1是）',
    query_type     varchar(200) default 'EQ' comment '查询方式（等于、不等于、大于、小于、范围）',
    html_type      varchar(200) comment '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
    dict_type      varchar(200) default '' comment '字典类型',
    sort           int comment '排序',
    create_by      varchar(64)  default '' comment '创建者',
    create_time    datetime comment '创建时间',
    update_by      varchar(64)  default '' comment '更新者',
    update_time    datetime comment '更新时间',
    primary key (column_id)
) engine = innodb comment = '代码生成业务表字段';